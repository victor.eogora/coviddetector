"""
imagePaths = list(paths.list_images(dataset))
data = []
labels = []

# Loop over the image paths
for imagePath in imagePaths:
    # Extract the class label from the file path (covid19 or no_covid19)
    label = imagePath.split(os.path.sep)[-2]

    image = cv2.imread(imagePath)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = cv2.resize(image, (224, 224))

    # Update the data and labels lists, respectively
    data.append(image)
    labels.append(label)

data = np.array(data) / 255.0
labels = np.array(labels)
"""