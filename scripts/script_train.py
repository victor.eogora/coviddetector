from coviddetector.train import ImportNpy, DataPreprocessing, Train
from coviddetector.models import Models
from coviddetector.predict import Predict

path = r"C:\Users\Victor\Google Drive\Work\BTS\Advanced Data Analysis\Gitlab\coviddectector\dataset"


"""
---------------- PIPELINE SECTION 1 ------------------------
"""
img, lab = ImportNpy(dataset_path=path).concatenate_arrays()

"""
---------------- PIPELINE SECTION 2 ------------------------
"""

trainX, testX, trainY, testY, lb = DataPreprocessing(images=img, labels=lab).split_train_test()

print(trainX.shape)
print(testX.shape)
print(trainY.shape)
print(testY.shape)

"""
---------------- PIPELINE SECTION 3 ------------------------
"""
m = Models(num_neurons_first=64, dropout=0.5, init_lr=1e-3, epochs=25).vgg16()
t = Train(model=m,
          trainX=trainX,
          trainY=trainY,
          testX=testX,
          testY=testY,
          rot_range=15,
          batch_size=8,
          epochs=1).train_fit_generator()

"""
---------------- PIPELINE SECTION 4 ------------------------
"""

p = Predict(model=m, testX=testX, testY=testY, batch_size=8, labels=lb).predict_covid()