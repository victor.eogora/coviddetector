from coviddetector.etl import DirTools, ImagePreprocessing

path = r"C:\Users\Victor\Google Drive\Work\BTS\Advanced Data Analysis\Gitlab\coviddectector\dataset"

covid, normal, _ = DirTools(path=path).get_dataset_paths()

covid_images, covid_labels = ImagePreprocessing(full_path=path, image_path=covid).get_images_to_array(save_to_npy=True)
normal_images, normal_labels = ImagePreprocessing(full_path=path, image_path=normal).get_images_to_array(save_to_npy=True)
