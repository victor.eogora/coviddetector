import numpy as np
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix


class Predict(object):

    def __init__(self, model, testX, testY, batch_size, labels):
        self.model = model
        self.testX = testX
        self.testY = testY
        self.batch_size = batch_size
        self.labels = labels

    def predict_covid(self):
        pred_ids = self.model.predict(self.testX, batch_size=self.batch_size)

        pred_ids = np.argmax(pred_ids, axis=1)

        print(classification_report(self.testY.argmax(axis=1), pred_ids, target_names=self.labels.classes_))

        return pred_ids
