import cv2
import numpy as np
import os
from numpy import save

class DirTools(object):
    """
    Class for Directory tools
    """

    def __init__(self, path):
        self.path = path

    def get_dataset_paths(self):
        """
        It gets Normal and Covid paths from base dataset
        :return: the paths to Covid and Normal datasets
        """

        global dir_covid
        global dir_normal
        dir_npy = None

        # Loop through directories, subdirs and files
        for dir, subdir, file in os.walk(self.path):

            # Register last folder
            last_folder = os.path.basename(os.path.normpath(dir))

            # Check if last folder is covid
            if last_folder == 'covid':
                dir_covid = dir

            # Check is last folder is normal
            elif last_folder == 'normal':
                dir_normal = dir

            # Check if last folder is npy
            elif last_folder == 'npy':
                dir_npy = dir

        return dir_covid, dir_normal, dir_npy

    def make_dir_if_not_exist(self):

        if not os.path.exists(self.path):
            os.makedirs(self.path)

class ImagePreprocessing(object):
    """
    Preprocess the Images for training
    """

    def __init__(self, full_path, image_path):
        self.full_path = full_path
        self.image_path = image_path

    def _save_npy(self, name, data):
        """
        Saves a NPY from data
        :param name: name with .npy extension
        :param data: numpy array
        :return: saved NPY
        """
        dir_out = os.path.join(self.full_path, "npy")
        DirTools(path=dir_out).make_dir_if_not_exist()
        save(os.path.join(dir_out, name), data)

    def get_images_to_array(self, save_to_npy=False):
        """
        Gets images and returns a list
        :return: A list of images and labels in a numpy array
        """

        images = []
        labels = []

        # Extract the class label from the file path (covid19 or normal)
        label = os.path.basename(os.path.normpath(self.image_path))

        # Loop through images and prepare them for training
        for image in os.listdir(self.image_path):

            print("Processing image :", image)

            impath = os.path.join(self.image_path, image)

            image = cv2.imread(impath)
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            image = cv2.resize(image, (224, 224))

            # Append to images
            images.append(image)

            # Append to labels
            labels.append(label)
            
        images = np.array(images) / 255.0
        labels = np.array(labels)

        if save_to_npy:
            self._save_npy(name="myarray_{}.npy".format(label), data=images)
            self._save_npy(name="mylabels_{}.npy".format(label), data=labels)

        return images, labels